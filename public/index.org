#+Title: Antwoorden leveren op hedendaagse vragen en uitdagingen voor morgen aangaan
#+Author: Frédéric Vanwindekens
#+Date: Waals centrum voor Landbouwkundig Onderzoek (CRA-W), Afdeling Duurzaamheid, systemen en vooruitzichten (D3), Eenheid Bodem, Water en Geïntegreerde Productie (U7)

#+Email: f.vanwindekens@cra.wallonie.be

#+STARTUP: overview
#+STARTUP: hidestars

#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'fade', slideNumber: true
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: white
#+REVEAL_HLEVEL: 1
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Org-Reveal Introduction.">
#+REVEAL_POSTAMBLE: <p> Created by yjwen. </p>
#+REVEAL_PLUGINS: (markdown notes)
#+REVEAL_EXTRA_CSS: ./frd_vnw.css
#+REVEAL_DEFAULT_SLIDE_BACKGROUND: #fff

#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
# +OPTIONS: reveal_single_file:t


# +REVEAL_INIT_OPTIONS: width:1200, height:800, controlsLayout: 'bottom-right', slideNumber: true, transition:'fade'


* Presentatie van de instelling
   :PROPERTIES:
   :reveal_background: #123456
   :END:

#+attr_html: :height 500px
[[./images/EAP_cra_image.png]]

** De geschiedenis van de instelling begon 150 jaar geleden
#+ATTR_REVEAL: :frag (fade-out)
[[./images/EAP_cra_historisch.png]]
#+REVEAL: split:t

#+ATTR_REVEAL: :frag (fade-out)
[[./images/EAP_cra_historisch2.png]]

** De geschiedenis van de instelling begon 150 jaar geleden
#+html: <p></p>
#+ATTR_REVEAL: :frag (roll-in)
#+ATTR_HTML: :style font-size:75%
- 1872 ::  Oprichting van de "Staatslaboratoria". In die tijd bestonden deze laboratoria uit 21 landbouwstations verspreid over heel België. Arthur Petermann was de eerste directeur van het landbouwstation van Gembloux. Zijn werk op fosfaatmeststoffen was heel belangrijk.
- 1937 :: Oprichting van het onderzoeksstation voor de verbetering van de aardappelteelt in de Hoge Ardennen
- 1951 :: Consolidatie van de Stations in twee Agronomische Onderzoekscentra: 12 in Gembloux en 9 in Gent
- 2001 :: Regionalisering van de landbouw: overdracht van landbouwbevoegdheden aan het Waals Gewest
- ... :: reorgansiatie ...
- 2022 :: 150 jaar !

** Het huidige CRA-W in enkele cijfers
#+ATTR_REVEAL: :frag (roll-in)
[[./images/EAP_cra_cijfers.png]]


#+ATTR_REVEAL: :frag (roll-in)
(*) broeikassen = /serres/ ; vestigingen = /bureaux/

** Waar bevinden onze gebouwen en velden zich?
#+ATTR_REVEAL: :frag (roll-in)
[[./images/EAP_cra_carte.png]]

** En hoe zien onze gebouwen eruit?
#+ATTR_REVEAL: :frag (roll-in)
[[./images/EAP_cra_batiments.png]]

#+ATTR_REVEAL: :frag (roll-in)
(*) zien eruit = ressembler

** De vijf onderzoeksdoelstellingen van het strategisch plan voor landbouwonderzoek in Wallonië

#+ATTR_REVEAL: :frag (roll-in)
- leefbare landbouwbedrijven geïntegreerd in levendige plattelandsgemeenschappen
- een landbouwaanpak die zich bekommert om de verwachtingen van de burger inzake consumentengezondheid, dierenwelzijn en milieu
- een actief landbouw- en bosbeheer om de gevolgen van de klimaatopwarming in te perken
- een actieve deelname van land- en bosbouwsectoren in de ontwikkeling van een biogebaseerde economie
- bruikbare en effectief gebruikte onderzoeksresultaten voor de belanghebbers

** leefbare landbouwbedrijven geïntegreerd in levendige plattelandsgemeenschappen
#+attr_html: :height 600px
[[./images/EAP_cra_planstra1.jpg]]

** een landbouwaanpak die zich bekommert om de verwachtingen van de burger inzake consumentengezondheid, dierenwelzijn en milieu
#+attr_html: :height 600px
[[./images/EAP_cra_planstra2.jpg]]

#+ATTR_REVEAL: :frag (roll-in)
(*) aanpak : /l'approche/ ; zich bekommeren : /se préoccuper/

** een actief landbouw- en bosbeheer om de gevolgen van de klimaatopwarming in te perken
#+attr_html: :height 600px
[[./images/EAP_cra_planstra3.jpg]]


#+ATTR_REVEAL: :frag (roll-in)
(*) gevolgen te perken : /atténuer les conséquences/

** een actieve deelname van land- en bosbouwsectoren in de ontwikkeling van een biogebaseerde economie
#+attr_html: :height 600px
[[./images/EAP_cra_planstra4.jpg]]

** bruikbare en effectief gebruikte onderzoeksresultaten voor de *belanghebbers* (*)
#+attr_html: :height 600px
[[./images/EAP_cra_planstra5.jpg]]

#+ATTR_REVEAL: :frag (roll-in)
(*) parties prenantes / stakeholders

** De opdracht van het CRA-W bestaat uit fundamenteel en toegepast onderzoek voor de landbouw- en de agrovoedingssector.
#+html: <p></p>
Het onderzoek zorgt voor *nuttige*, *bruikbare*, *duurzame* en *rendabele* oplossingen en verbeteringen voor conventionele en biologische landbouw

#+BEGIN_SRC sh :results silent :eval t :exports none
   montage /home/fred/Images/Photos/PhotosPractices/julien_bunkens_drone_hesbaye/Bunkens2021_sillon_pomme_de_terre_drone.jpg \
	   /home/fred/Images/Photos/PhotosPractices/Serveau2019_Corbais_drone.JPG \
	     -tile x1 -geometry x500-20-50 -background none \
	     ./images/EAP_cra_conv_bio.png
#+END_SRC
[[./images/EAP_cra_conv_bio.png]]

** In vier grote onderzoeksdomeinen
#+html: <p></p>
#+ATTR_REVEAL: :frag (roll-in)
- Precisielandbouw
- Precisieveeteelt
- Risicobeheersing
- De producten kennen

** Precisielandbouw - ex. AgRoboConnect : 
#+attr_html: :height 400px
[[./images/EAP_cra_proj3.jpg]]

#+ATTR_REVEAL: :frag (roll-in)
Duurzame en ecologische landbouw vergt meer werk, vooral op het gebied van het monitoren en bestrijden van onkruid. Robots zijn een manier om de last voor boeren te verlichten.

** Precisieveeteelt - ex. WALLBOVINUT : 
#+attr_html: :height 400px
[[./images/EAP_cra_proj2.png]]

#+ATTR_REVEAL: :frag (roll-in)
Inzet van digitale hulpmiddelen voor veerantsoenering op het WALLeSMART-platform

** Risicobeheersing - ex. TOMORR'HOP : 
#+attr_html: :height 400px
[[./images/EAP_cra_proj1.png]]

#+ATTR_REVEAL: :frag (roll-in)
Innovatieve oplossingen voor hopteelt in een context van klimaatverandering en agro-ecologische overgang 

** De producten kennen - ex. TechnoCerBio : 

#+attr_html: :height 400px
[[./images/EAP_cra_proj4.jpg]]

#+ATTR_REVEAL: :frag (roll-in)
Technologische kwaliteit van voedingsgranen met betrekking tot rassenkeuze, stikstofbemesting en gewasbeschermingsmiddelen in de biologische landbouw

** Een grapje (maar echte :-/ ) : ex. REFLECHI

#+ATTR_REVEAL: :frag (roll-in)
*RE*ndre les cultures maraîchères, *F*ruitières et les *LE*gumes d’industrie plus résilients au *CH*angement cl*I*matique

** Administratief organigram - Waar ben ik ?

[[./images/EAP_cra_organigram_direction.png]]

#+REVEAL: split:t
[[./images/EAP_cra_organigram_recherche.png]]

#+REVEAL: split:t
[[./images/EAP_cra_organigram_recherche2.png]]


#+BEGIN_SRC sh :results silent :eval t :exports none
   montage /home/fred/Code/Slides/slides-now/public/images/EAP_cra_sol1.jpg \
/home/fred/Code/Slides/slides-now/public/images/EAP_cra_sol2.jpg \
	     -tile x1 -geometry x500-20-50 -background none \
	     ./images/EAP_cra_sol3.png
#+END_SRC

#+ATTR_REVEAL: :frag (roll-in)
Beoordeling van de prestaties van innovatieve teeltsystemen en hun impact op bodem, water en luchtbronnen.

#+ATTR_REVEAL: :frag (roll-in)
[[./images/EAP_cra_sol3.png]]



* Presentatie van mijn onderzoeken : P.I.R.A.T.
   :PROPERTIES:
   :reveal_background: #123456
   :END:

#+html: <p></p>
#+ATTR_REVEAL: :frag (roll-in)
praktijken innovatieve en de *veerkracht* (*) van agroecosystemen in transitie

#+html: <p></p>
#+ATTR_REVEAL: :frag (roll-in)
(*) résilience

** Sociaal-ecologische systemen

#+ATTR_REVEAL: :frag (roll-in)
Geïntegreerd systeem van ecosystemen en de menselijke samenleving met *wederzijdse terugkoppeling* (1) en *onderlinge afhankelijkheid* (2). Het concept benadrukt het perspectief mens-in-natuur

#+ATTR_HTML: :style text-align:right
#+ATTR_HTML: :style font-size:50%
- Walker, 2010

#+ATTR_REVEAL: :frag roll-in
- (1) boucle de rétro-action / feedback
- (2) interdépendances
  
#+REVEAL: split:t
[[./images/agriculture.jpg]]
#+REVEAL: split:t
[[./images/forest.jpg]]
#+REVEAL: split:t
 [[./images/fisheries.jpg]]
#+REVEAL: split:t
[[./images/worldviews.png]]

** Cognitief kaart, individueel and sociaal
   [[./images/mm_socmap_pap1.png]]

   #+ATTR_HTML: :style font-size:50%
   Vanwindekens, F.M., Stilmant, D. & Baret, P.V. (2013). Development of a broadened cognitive mapping approach for analysing systems of practices in social-ecological systems. Ecological Modelling 250 : 352-362.

#+REVEAL: split:t
[[./images/EAP_cra_scholar.png]]

#+REVEAL: split:t
[[./images/cogmap1.png]]
#+REVEAL: split:t
[[./images/cogmap2.png]]
#+REVEAL: split:t
[[./images/cogmap3.png]]

** Toepassing van deze methode
#+html: <p></p>
#+ATTR_REVEAL: :frag (roll-in)
- veehouders in de Ardennen en de Famenne
- Italiaanse wijnboeren
- Franse bijenhouder
- bodembiodiversiteit in vlaamse en roemeense bossen
- Europese bodembeheerpraktijken (landbouw)

** OpenSource
[[./images/hex_cogmapr.png]]

* Een woord over de geschiedenis van grondbewerking
   :PROPERTIES:
   :reveal_background: #123456
   :END:
    #+attr_html: :height 500px
    [[./images/Mazoyer2002.jpg]]

** Tijdens de oudheid

[[./images/ANT_beche_houe.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mazoyer & Roudart, 2002

** Tijdens de oudheid

[[./images/ANT_araires.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mazoyer & Roudart, 2002
    
** Tijdens de oudheid
[[./images/ANT_attelage.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mazoyer & Roudart, 2002

** Tijdens de Middeleeuwen
[[./images/MA_attelage.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mazoyer & Roudart, 2002

** Tijdens de Middeleeuwen
[[./images/MA_charrue.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mazoyer & Roudart, 2002

** Tijdens de Middeleeuwen
[[./images/MA_jachere_labouree.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mazoyer & Roudart, 2002

** De Nieuwe Tijd
    #+attr_html: :height 500px
    [[./images/TM_brabant_double.png]]
    #+ATTR_HTML: :style font-size:40% ; text-align:right
    Bron: Mazoyer & Roudart, 2002

** In de jaren '30: Dust Bowl, Texas (1935)

[[./images/1280px-Dust_Storm_Texas_1935.jpg]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Wikimedia, NOAA George E. Marsh Album, theb1365, Historic C&GS Collection, Public domain

** In de jaren '30: Dust Bowl, S. Dakota (1936)

[[./images/Dust_Bowl_-_Dallas,_South_Dakota_1936.jpg]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Wikimedia, Public domain


** Landbouwpraktijken en -productie
    #+attr_html: :height 500px
    [[./images/Bunkens2021_travail_sol_deux_tracteurs.png]]
    #+ATTR_HTML: :style font-size:40% ; text-align:right
    Copyright ©Julien Bunkens, 2021

  
** Impact van landbouwpraktijken op het milieu...
    [[./images/Houben2021_Erosion_Belgium1.jpg]]
    #+ATTR_HTML: :style font-size:40% ; text-align:right
    Copyright ©Patrick Houben, 2021

** Impact van landbouwpraktijken op het milieu...
    [[./images/Houben2021_Erosion_Belgium2.jpg]]
    #+ATTR_HTML: :style font-size:40% ; text-align:right
    Copyright ©Patrick Houben, 2021
   
** ... & impact van mondiale veranderingen op landbouwproductie
    [[./images/sécheresse2018.jpg]]
    #+ATTR_HTML: :style font-size:40% ; text-align:right
    Copyright ©Le Sillon Belge, bron: www.sillonbelge.be/2898/article/2018-08-09/rendement-et-qualite-des-pommes-de-terre-la-secheresse-et-la-canicule-pesent
  
** Behoefte aan indicatoren...
... pragmatisch en eenvoudig om:
- de gezondheid en kwaliteit van een bodem te evalueren
- de invloed van landbouwpraktijken en milieufactoren te *beoordelen* (1)

#+ATTR_REVEAL: :frag (roll-in)
(1) évaluer

* De structuur van de bodem en zijn stabiliteit...
   :PROPERTIES:
   :reveal_background: #123456
   :END:
    #+attr_html: :height 500px
   [[./images/Soil-structure_USDA.png]]
   #+ATTR_HTML: :style font-size:40% ; text-align:right
   Bron: U.S. Department of Agriculture
   
** De textuur van de bodem
[[./images/triangle_textural_belge_1954_legrain.png]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: ©Legrain, 2019, bron: Mémoire Clément Masson

** De structuur van de bodem

De *bodemstructuur* is de manier van *samenstelling* van mineraaldeeltjes (klei, slib en zand) en humus. Wanneer deze samenkomen, vormen de minerale deeltjes en humus *aggregaten*. De structuur is een *kortetermijneigenschap* (1) van de bodem. Ze hangt direct af van de textuur, maar ook van de hoeveelheid colloïden, het watergehalte en de activiteit van het bodemleven. De structuur heeft een grote invloed op de *porositeit*.

#+ATTR_REVEAL: :frag (roll-in)
(1) une propriété variable à court terme

#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: Mémoire Clément Masson

** De structuur van de bodem in de natuur
   #+attr_html: :height 500px
   [[./images/Soil-structure_USDA.png]]
   #+ATTR_HTML: :style font-size:40% ; text-align:right
   Bron: U.S. Department of Agriculture

** De structuur van de bodem (beeld zoom +/- 20X, op papier)

#+attr_html: :height 500px
[[./images/SoilStructure.jpg]]
#+ATTR_HTML: :style font-size:40% ; text-align:right
Bron: =https://symbiosis.co.nz/natures-teaching/soil-health/soil-structure/=

** Het belang van structuur in de landbouw
#+ATTR_REVEAL: :frag (roll-in)
   * *Ontwikkeling* (1) van porositeit: micro-, macroporiën
   * *Opslag en afvoer* (2) van water
   * Beluchting, *ademhaling* (3), gasuitwisseling
   * Leefomgeving voor symbiotische organismen (of niet...): mycorrhizae, bodemdieren, ...
   * *Wortel* (4)ontwikkeling van gewassen (of niet)
   * Gezondheid van gewassen
   * en ... *landbouwproductie*
      
#+ATTR_REVEAL: :frag (roll-in)
(1) Développement, (2) stockage et drainage, (3) respiration, (4) Racines


** De Slake Test bij CRA-W

#+attr_html: :height 500px
[[./images/test_colonnes.png]]

** QuantiSlakeTest: een nieuwe methode voor de analyse van de *structurele stabiliteit*


   # +ATTR_LATEX: :width 0.5\linewidth
   # +ATTR_HTML: :width 33% :height 33%
   [[./images/schema_expe_graph.png]]

   #+REVEAL: split:t
   #+REVEAL_HTML: <p><iframe width="560" height="315" src="https://www.youtube.com/embed/VETSN8XmmuY?si=DQ4oghRHC12TDtdP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe></p>
   https://www.youtube.com/watch?v=VETSN8XmmuY



   
** QuantiSlakeTest: We gaan internationaal...
[[./images/slaking_lab_cotes.png]]

** OpenSource
[[./images/hex_slaker.png]]

* Conferenties
   :PROPERTIES:
   :reveal_background: #123456
   :END:

   #+attr_html: :height 500px
   [[./images/map_mission.png]]

* Andere themas : Koolstof in de bodem in verband met klimaatverandering en de vermindering van CO2-uitstoot
   :PROPERTIES:
   :reveal_background: #123456
   :END:

   #+attr_html: :height 500px
   [[./images/station_petroliere_feu.jpg]]

   #+ATTR_HTML: :style font-size:40% ; text-align:right
   Copyright ©The New York Times (& British Petroleum...)

